export class Metaier {

    static createElement(properties) {
        const metaDom = document.createElement('meta');

        for (const key in properties) {
            if (key && properties[key]) {
                metaDom.setAttribute(key, properties[key]);
            }
        }

        document.head.appendChild(metaDom);
        return metaDom;
    }

    static deleteElement(properties) {
        let status = false;
        for (const key in properties) {
            const elements = document.querySelectorAll(`meta[${key}="${properties[key]}"]`);
            [].forEach.call(elements, element => {
                element.remove();
                status = true;
            });
        }
        return status;
    }

}
